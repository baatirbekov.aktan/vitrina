<?php

namespace Database\Factories;

use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Shop>
 */
class ShopFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'address' => $this->faker->address(),
            'description' => $this->faker->paragraph(3),
            'image' => $this->faker->name,
            'url' => $this->faker->url(),
            'coordinates' => $this->faker->name,
            'contacts' => $this->faker->phoneNumber(),
            'work_time' => $this->faker->word(),
        ];
    }
}
