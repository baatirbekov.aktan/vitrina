<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductImage>
 */
class ProductImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'image' => $this->getImage(rand(1, 31))
        ];
    }

    private function getImage($image_number): string
    {
        $path = storage_path() . "/images/products/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(500)->encode('jpg');
        Storage::disk('public')->put('images/products/' . $image_name, $resize->__toString());
        return 'images/products/' . $image_name;
    }
}
