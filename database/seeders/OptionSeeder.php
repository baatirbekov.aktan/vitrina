<?php

namespace Database\Seeders;

use App\Models\Option;
use App\Models\OptionValue;
use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Option::factory()->state(['name' => 'Цвет', 'type' => 'color', 'value' => 'color'])->has(
            OptionValue::factory()->state(['name' => 'Синий','value' => '#4287f5' ])
        )->has(
            OptionValue::factory()->state(['name' => 'Белый', 'value' => '#f2f5f0',])
        )->has(
            OptionValue::factory()->state(['name' => 'Красный', 'value' => '#e81b00',])
        )->has(
            OptionValue::factory()->state(['name' => 'Черный', 'value' => '#000000',])
        )->has(
            OptionValue::factory()->state(['name' => 'Зеленый', 'value' => '#00ff00',])
        )->create();

        Option::factory()->state(['name' => 'Размер', 'type' => 'text', 'value' => 'size'])->has(
            OptionValue::factory()->state(['name' => 'S'])
        )->has(
            OptionValue::factory()->state(['name' => 'M'])
        )->has(
            OptionValue::factory()->state(['name' => 'L'])
        )->has(
            OptionValue::factory()->state(['name' => 'XL'])
        )->has(
            OptionValue::factory()->state(['name' => 'XXL'])
        )->create();

        Option::factory()->state(['name' => 'Материал', 'type' => 'text', 'value' => 'material'])->has(
            OptionValue::factory()->state(['name' => 'Полиэстер'])
        )->has(
            OptionValue::factory()->state(['name' => 'Хлопок'])
        )->has(
            OptionValue::factory()->state(['name' => 'Полиэфирное волокно'])
        )->has(
            OptionValue::factory()->state(['name' => 'Искусственная кожа'])
        )->has(
            OptionValue::factory()->state(['name' => 'Полиэфир'])
        )->has(
            OptionValue::factory()->state(['name' => 'Акрил'])
        )->has(
            OptionValue::factory()->state(['name' => 'Вельвет'])
        )->has(
            OptionValue::factory()->state(['name' => 'Деним'])
        )->has(
            OptionValue::factory()->state(['name' => 'Драп'])
        )->has(
            OptionValue::factory()->state(['name' => 'Кожа'])
        )->has(
            OptionValue::factory()->state(['name' => 'Лен'])
        )->create();
    }
}
