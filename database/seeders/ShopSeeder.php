<?php

namespace Database\Seeders;

use App\Models\Shop;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 5; $i++) {
            Shop::factory()->state(['image' => $this->getImage($i)])->create();
        }
    }

    private function getImage($image_number): string
    {
        $path = storage_path() . "/images/shops/" . $image_number . ".png";
        $image_name = md5($path) . '.png';
        $resize = Image::make($path)->fit(500)->encode('png');
        Storage::disk('public')->put('images/shops/' . $image_name, $resize->__toString());
        return 'images/shops/' . $image_name;
    }
}
