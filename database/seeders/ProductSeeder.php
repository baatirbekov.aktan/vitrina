<?php

namespace Database\Seeders;

use App\Models\Option;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Shop;
use App\Models\Subcategory;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $shops = Shop::all();
        $subcategories = Subcategory::all();


        foreach ($subcategories as $subcategory) {
            $option = Option::query()->inRandomOrder()->first();
            $optionsIds = $option->inRandomOrder()->limit(5)->get()->pluck('id');
            foreach ($shops as $shop) {
                Product::factory()->count(3)->state(['options' => [$option->id => $optionsIds]])->for($shop)->for($subcategory)->has(
                    ProductImage::factory()->count(4)
                )->create();
            }
        }

    }
}
