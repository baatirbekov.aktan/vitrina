<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Gender;
use App\Models\Option;
use App\Models\Subcategory;
use Illuminate\Database\Seeder;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $femaleWearSubcategories = [
            'Блузки и рубашки',
            'Брюки',
            'Верхняя одежда',
            'Джемперы, водолазки и кардиганы',
            'Джинсы',
            'Комбинезоны',
            'Костюмы',
            'Лонгсливы',
            'Пиджаки, жилеты и жакеты',
            'Белье',
            'Футболки',
            'Спортивные костюмы',
        ];

        $maleWearSubcategories = [
            'Рубашки',
            'Брюки',
            'Верхняя одежда',
            'Джемперы, водолазки и кардиганы',
            'Джинсы',
            'Комбинезоны',
            'Костюмы',
            'Лонгсливы',
            'Пиджаки, жилеты и жакеты',
            'Белье',
            'Футболки',
            'Спортивные костюмы',
        ];

        $femaleShoesSubcategories = [
            'Ботинки',
            'Домашняя обувь',
            'Кроссовки',
            'Кеды',
            'Сандали',
            'Сапоги',
            'Туфли',
            'Экспандрильи',
        ];

        $maleShoesSubcategories = [
            'Ботинки',
            'Домашняя обувь',
            'Кроссовки',
            'Кеды',
            'Сандали',
            'Сапоги',
            'Туфли',
            'Экспандрильи',
        ];

        $femaleAccessoriesSubcategories = [
            'Перчатки и варежки',
            'Сумки',
            'Укращения',
            'Косметички',
            'Головные уборы',
            'Шарфы',
        ];

        $maleAccessoriesSubcategories = [
            'Галстуки',
            'Запонки',
            'Зонты',
            'Очки',
            'Часы',
            'Шарфы',
        ];

        $wearCategory = Category::factory()->state(['name' => 'Одежда'])->create();
        $shoesCategory = Category::factory()->state(['name' => 'Обувь'])->create();
        $accessoriesCategory = Category::factory()->state(['name' => 'Аксессуары'])->create();

        $maleGender = Gender::factory()->state(['name' => 'Мужчинам'])->create();
        $femaleGender = Gender::factory()->state(['name' => 'Женщинам'])->create();

        $options = Option::all();

        foreach ($femaleWearSubcategories as $subcategory) {
            Subcategory::factory()->state(['name' => $subcategory])->for($wearCategory)->for($femaleGender)->hasAttached($options)->create();
        }

        foreach ($femaleAccessoriesSubcategories as $subcategory) {
            Subcategory::factory()->state(['name' => $subcategory])->for($accessoriesCategory)->for($femaleGender)->hasAttached($options)->create();
        }

        foreach ($femaleShoesSubcategories as $subcategory) {
            Subcategory::factory()->state(['name' => $subcategory])->for($shoesCategory)->for($femaleGender)->create();
        }

        foreach ($maleWearSubcategories as $subcategory) {
            Subcategory::factory()->state(['name' => $subcategory])->for($wearCategory)->for($maleGender)->hasAttached($options)->create();
        }

        foreach ($maleAccessoriesSubcategories as $subcategory) {
            Subcategory::factory()->state(['name' => $subcategory])->for($accessoriesCategory)->for($maleGender)->hasAttached($options)->create();
        }

        foreach ($maleShoesSubcategories as $subcategory) {
            Subcategory::factory()->state(['name' => $subcategory])->for($shoesCategory)->for($maleGender)->hasAttached($options)->create();
        }
    }
}
