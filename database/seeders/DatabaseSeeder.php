<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(10)->create();

        $this->call(RoleSeeder::class);

        $adminRole = Role::query()->where('name', 'admin')->first();

        \App\Models\User::factory()->create([
            'email' => 'admin@admin.com',
        ])->roles()->attach($adminRole->id);

        $this->call(OptionSeeder::class);
        $this->call(SubcategorySeeder::class);
        $this->call(ShopSeeder::class);
        $this->call(ProductSeeder::class);
    }
}
