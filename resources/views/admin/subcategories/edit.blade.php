@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            Edit
        </div>
        <div class="card-body">
            <form class="row g-3" method="post" action="{{ route('admin.subcategories.update', ['subcategory' => $subcategory]) }}">
                @csrf
                @method('PUT')
                <div class="col-md-6">
                    <x-forms.input label="Название" id="name" name="name" type="text"
                                   value="{{ $subcategory->name }}">
                    </x-forms.input>
                </div>
                <div class="col-md-6">
                    <x-forms.select class="single-select" name="category_id" label="Категория"
                                    placeholder="Выберите категорию"
                                    id="category_id">
                        @foreach($categories as $category)
                            <option @selected($subcategory->category_id  == $category->id) value="{{ $category->id }}">
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </x-forms.select>
                </div>
                <div class="col-md-6">
                    <x-forms.select multiple name="options[]" label="Варианты"
                                    id="options" class="multiple-select">
                    @foreach($options as $option)
                            <option @selected(in_array($option->id, $subcategoryOptions)) value="{{ $option->id }}">
                                {{ $option->name }}
                            </option>
                        @endforeach
                    </x-forms.select>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
