@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            Edit
        </div>
        <div class="card-body">
            <form class="row g-3" method="post" action="{{ route('admin.products.update', ['product' => $product]) }}">
                @csrf
                @method('PUT')
                <div class="col-md-6">
                    <x-forms.input label="Название" id="name" name="name" type="text"
                                   value="{{ $product->name }}">
                    </x-forms.input>
                </div>
                <div class="col-md-6">
                    <x-forms.input label="Цена" id="price" name="price" type="number"
                                   value="{{ $product->price }}">
                    </x-forms.input>
                </div>
                <div class="col-md-6">
                    <x-forms.select class="single-select" name="subcategory_id" label="Подкатегория"
                                    placeholder="Выберите подкатегорию"
                                    id="subcategory_id">
                        @foreach($subcategories as $subcategory)
                            <option
                                @selected($product->subcategory_id  == $subcategory->id) value="{{ $subcategory->id }}">
                                {{ $subcategory->name }}
                            </option>
                        @endforeach
                    </x-forms.select>
                </div>
                <div class="col-md-6">
                    <x-forms.select class="single-select" name="shop_id" label="Магазин"
                                    placeholder="Выберите магазин"
                                    id="shop_id">
                        @foreach($shops as $shop)
                            <option @selected($product->shop_id  == $shop->id) value="{{ $shop->id }}">
                                {{ $shop->name }}
                            </option>
                        @endforeach
                    </x-forms.select>
                </div>
                @foreach($product->subcategory->options as $option)
                    <div class="col-md-6">
                        <x-forms.select multiple name="options[{{ $option->value }}][]" label="{{ $option->name }}"
                                        id="options[{{ $option->id }}][]" class="multiple-select">
                            @foreach($option->optionValues as $optionValue)
                                <option @selected(in_array($optionValue->id, $product->options[$option->value] ?? [])) value="{{ $optionValue->id }}">
                                    {{ $optionValue->name }}
                                </option>
                            @endforeach
                        </x-forms.select>
                    </div>
                @endforeach
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
