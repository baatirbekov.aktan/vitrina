@extends('layouts.admin')

@section('content')
    <form action="{{ route('admin.users.index') }}" id="search" method="get" data-table="users"></form>

    <div class="card">
        <div class="card-header">
            Users
        </div>
        <div class="card-body">
            <div class="d-flex justify-content-between mb-3">
                <div>
                    <select name="limit" form="search" id="limit" style="width: 100px;" class="form-select"
                            aria-label="Default select example">
                        @foreach([5, 10, 20] as $value)
                            <option @if(Session::get("{$table}.limit") == $value ) selected @endif value="{{ $value }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <div class="input-group">
                        <input form="search" value="{{ request()->get('search') }}" type="text" class="form-control"
                               name="search" placeholder="Search">
                        <button data-name="search" class="btn btn-outline-danger clean-input">
                            <i class="fa-solid fa-xmark"></i>
                        </button>
                    </div>
                </div>
            </div>

            @include('admin.users.table', ['users' => $users])

        </div>
    </div>
@endsection
