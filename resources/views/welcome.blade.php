<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Главная | ВИТРИНА - маркетплейс одежды и обуви</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&amp;display=swap&amp;subset=cyrillic"
          rel="stylesheet">


    <script src="{{ asset('js/vendors.d95a6e5831869bd5a585.js') }}"></script>
    <script src="{{ asset('js/app.39e71629810621a4b505.js') }}"></script>
    <link href="{{ asset('css/vendors.bdf213427bafb9a38bbc.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.a72417657e89e7791e2e.css') }}" rel="stylesheet">
</head>
<body class="home container">

<div class="l-wrap">
    <div id="navigation"></div>
    <div class="topline">
        <div class="topline__row row">
            <div class="topline__left col-auto"><a class="link link--icon" href="#" data-fancybox
                                                   data-src="#my-location">
                    <svg class="icon icon-location">
                        <use xlink:href="img/symbol-defs.svg#icon-location"></use>
                    </svg>
                    <span>Владивосток</span></a></div>
            <div class="topline__right col-auto"><a class="link" href="#">Сотрудничество</a><a class="link"
                                                                                               href="#">Помощь</a>
            </div>
        </div>

    </div>
    <header class="top">
        <div class="top__row row">
            <div class="top__logo col-auto">
                <div class="logo"><img class="logo__img" src="./img/logo.svg" width="110" height="40"><span
                        class="logo__caption">Маркетплейс <br>одежды и обуви</span></div>
            </div>
            <div class="top__catalog col-md-auto"><a class="nav-cat" href="catalog.html">
                    <svg class="icon icon-catalog">
                        <use xlink:href="img/symbol-defs.svg#icon-catalog"></use>
                    </svg>
                    <span>Каталог</span></a></div>
            <div class="top__search col-md"><a class="top__search-back d-md-none" href="#"
                                               onclick="history.back();return false;">
                    <svg class="icon icon-chevron-left">
                        <use xlink:href="#"></use>
                    </svg>
                </a>
                <div class="input-group">
                    <div class="select-custom unselectable">
                        <select class="form-select">
                            <option selected>По товарам</option>
                            <option>По категориям</option>
                        </select>
                    </div>
                    <button class="input-group-icon" type="submit" onclick="window.location='catalog-search.html'">
                        <svg class="icon icon-search">
                            <use xlink:href="#"></use>
                        </svg>
                    </button>
                    <input class="form-control" type="text" placeholder="Поиск на Витрине">
                </div>
            </div>
            <div class="top__tools col-auto"><a class="minicart" href="cart.html">
                    <div class="minicart__icon icount">
                        <svg class="icon icon-cart">
                            <use xlink:href="img/symbol-defs.svg#icon-cart"></use>
                        </svg>
                        <span class="minicart__count icount__caption">12</span>
                    </div>
                    <span class="minicart__cost"><span id="minicart-cost">101 999</span>₽</span></a>
                <div class="top__auth"><a class="link link--icon" href="#" data-fancybox
                                          data-src="#auth-phone-popup">
                        <svg class="icon icon-profile">
                            <use xlink:href="img/symbol-defs.svg#icon-profile"></use>
                        </svg>
                        <span>Войти</span></a></div>
                <div class="top__hamb col-auto">
                    <div></div>
                </div>
            </div>
        </div>
    </header>
    <div class="mpanel d-md-none">
        <div class="mpanel__row"><a class="mpanel__item" href="#">
                <div class="mpanel__iconwrap">
                    <svg class="icon icon-home">
                        <use xlink:href="img/symbol-defs.svg#icon-home"></use>
                    </svg>
                </div>
                <span class="mpanel__caption">Главная</span></a><a class="mpanel__item" href="#">
                <div class="mpanel__iconwrap">
                    <svg class="icon icon-catalog">
                        <use xlink:href="img/symbol-defs.svg#icon-catalog"></use>
                    </svg>
                </div>
                <span class="mpanel__caption">Каталог</span></a><a class="mpanel__item" href="cart.html">
                <div class="mpanel__iconwrap">
                    <svg class="icon icon-cart">
                        <use xlink:href="img/symbol-defs.svg#icon-cart"></use>
                    </svg>
                </div>
                <span class="mpanel__caption">Корзина</span></a><a class="mpanel__item" href="#">
                <div class="mpanel__iconwrap">
                    <svg class="icon icon-profile">
                        <use xlink:href="img/symbol-defs.svg#icon-profile"></use>
                    </svg>
                </div>
                <span class="mpanel__caption">Кабинет</span></a></div>
    </div>
    <div class="promo">
        <div class="swiper-body swiper-stl1">
            <div class="promo__slider swiper slider-nav-pag">
                <div class="swiper-wrapper">
                    <div class="promo-slide swiper-slide pslide1"><a class="promo__slider-wrap" href="#"> <img
                                class="promo__slider-img l-desktop" src="./img/slides/promo1_desktop.jpg"><img
                                class="promo__slider-img l-mobile" src="./img/slides/promo1_mobile.jpg"></a></div>
                    <div class="promo-slide swiper-slide pslide1"><a class="promo__slider-wrap" href="#"> <img
                                class="promo__slider-img l-desktop" src="./img/slides/promo1_desktop.jpg"><img
                                class="promo__slider-img l-mobile" src="./img/slides/promo1_mobile.jpg"></a></div>
                </div>
            </div>
            <div class="swiper-nav swiper-nav--mobile">
                <div class="swiper-button swiper-prev">
                    <div class="d-md-none">
                        <svg class="icon icon-arrow-left-small">
                            <use xlink:href="img/symbol-defs.svg#icon-arrow-left-small"></use>
                        </svg>
                    </div>
                    <svg class="icon icon-slide-left">
                        <use xlink:href="img/symbol-defs.svg#icon-slide-left"></use>
                    </svg>
                </div>
                <div class="swiper-button swiper-next">
                    <div class="d-md-none">
                        <svg class="icon icon-arrow-right-small">
                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right-small"></use>
                        </svg>
                    </div>
                    <svg class="icon icon-slide-right">
                        <use xlink:href="img/symbol-defs.svg#icon-slide-right"></use>
                    </svg>
                </div>
            </div>
            <div class="swiper-pagination swiper-pagination--mobile"></div>
        </div>
    </div>
    <div class="catsections">
        <div class="catsections__row row">
            @foreach(\App\Models\Category::all() as $category)
            <div class="catsection col col-lg-4"><a class="catsection__wrap" href="{{ route('categories.show', ['category' => $category]) }}"
                                                    style="background-color: #DEDFE0;
                                                        background-image: url(&quot;./img/catalog/section1.jpg&quot;)">
                    <div class="catsection__body">
                        <div class="catsection__iconwrap">
                            <svg class="icon icon-cloth">
                                <use xlink:href="img/symbol-defs.svg#icon-cloth"></use>
                            </svg>
                        </div>
                        <span class="catsection__title">{{ $category->name }}</span>
                    </div>
                </a></div>
            @endforeach
        </div>
    </div>
    <div class="prods prods--main-discount i-catview-parent">
        <div class="section-head">
            <div class="section-title">Товары со скидкой</div>
            <a class="link link--more d-none d-lg-inline-flex" href="#"><span>Больше скидок</span>
                <svg class="icon icon-arrow-right">
                    <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                </svg>
            </a>
            <div class="catview d-lg-none">
                <div class="catview__btn i-catview-toggle v1 is-active"><span
                        class="catview__btn--v3"><span></span><span></span><span></span></span>
                </div>
                <div class="catview__btn i-catview-toggle v2"><span
                        class="catview__btn--v6"><span> </span><span> </span><span> </span><span> </span><span> </span><span></span></span>
                </div>
            </div>
        </div>
        <div class="prods__row row i-catview-container">
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product1.jpg"
                            alt="Туфли Майк Мери">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product1.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Обувь</a><span
                                class="vlabel vlabel--d-30"><span>Скидка </span>30%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Туфли Майк Мери</a></div>
                        <div class="prod__prices"><span class="prod__price">1 853 ₽</span><span
                                class="prod__oldprice">2 647</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product2.jpg"
                            alt="Кардиган Абайхан">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product2.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Одежда</a><span
                                class="vlabel vlabel--d-20"><span>Скидка </span>20%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Кардиган Абайхан</a></div>
                        <div class="prod__prices"><span class="prod__price">720 ₽</span><span
                                class="prod__oldprice">864</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product3.jpg"
                            alt="Наручные часы expert of success">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product3.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Аксессуары</a><span
                                class="vlabel vlabel--d-50"><span>Скидка </span>50%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Наручные часы expert of success</a></div>
                        <div class="prod__prices"><span class="prod__price">449 ₽</span><span
                                class="prod__oldprice">898</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product4.jpg"
                            alt="Ботинки Tervolina">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product4.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Обувь</a><span
                                class="vlabel vlabel--d-30"><span>Скидка </span>30%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Ботинки Tervolina</a></div>
                        <div class="prod__prices"><span class="prod__price">2 647 ₽</span><span
                                class="prod__oldprice">3 596</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product5.jpg"
                            alt="Блузка VELOCITY">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product5.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Одежда</a><span
                                class="vlabel vlabel--d-10"><span>Скидка </span>10%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Блузка VELOCITY</a></div>
                        <div class="prod__prices"><span class="prod__price">700 ₽</span><span
                                class="prod__oldprice">864</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product6.jpg"
                            alt="Брошь СТРЕКОЗА">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product6.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Аксессуары</a><span
                                class="vlabel vlabel--d-40"><span>Скидка </span>40%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Брошь СТРЕКОЗА</a></div>
                        <div class="prod__prices"><span class="prod__price">139 ₽</span><span
                                class="prod__oldprice">898</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product7.jpg"
                            alt="Кеды Vans">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product7.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Обувь</a><span
                                class="vlabel vlabel--d-60"><span>Скидка </span>60%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Кеды Vans</a></div>
                        <div class="prod__prices"><span class="prod__price">2 299 ₽</span><span
                                class="prod__oldprice">2 647</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod prodb col-lg-3">
                <div class="swiper-body swiper-stl1 swiper-stl1--prodb">
                    <div class="prodb__slider swiper slider-pag">
                        <div class="swiper-wrapper">
                            <div class="prodb-slide swiper-slide"><a class="slider-link" href="#"><img
                                        class="prodb__img d-none d-sm-block"
                                        src="./img/slides/sale_futbolki1_desktop.jpg"></a><a class="slider-link"
                                                                                             href="#"><img
                                        class="prodb__img d-block d-sm-none"
                                        src="./img/slides/sale_futbolki1_mobile.jpg"></a></div>
                            <div class="prodb-slide swiper-slide"><a class="slider-link" href="#"><img
                                        class="prodb__img d-none d-sm-block"
                                        src="./img/slides/sale_futbolki1_desktop.jpg"></a><a class="slider-link"
                                                                                             href="#"><img
                                        class="prodb__img d-block d-sm-none"
                                        src="./img/slides/sale_futbolki1_mobile.jpg"></a></div>
                            <div class="prodb-slide swiper-slide"><a class="slider-link" href="#"><img
                                        class="prodb__img d-none d-sm-block"
                                        src="./img/slides/sale_futbolki1_desktop.jpg"></a><a class="slider-link"
                                                                                             href="#"><img
                                        class="prodb__img d-block d-sm-none"
                                        src="./img/slides/sale_futbolki1_mobile.jpg"></a></div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product8.jpg"
                            alt="Тапочки ИвШуз">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product8.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Обувь</a><span
                                class="vlabel vlabel--d-30"><span>Скидка </span>30%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Тапочки ИвШуз</a></div>
                        <div class="prod__prices"><span class="prod__price">350 ₽</span><span
                                class="prod__oldprice">2 647</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product9.jpg"
                            alt="Брюки Eva Cambru">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product9.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Одежда</a><span
                                class="vlabel vlabel--d-20"><span>Скидка </span>20%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Брюки Eva Cambru</a></div>
                        <div class="prod__prices"><span class="prod__price">864 ₽</span><span
                                class="prod__oldprice">1 146</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product10.jpg"
                            alt="Панама NOVO">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product10.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Аксессуары</a><span
                                class="vlabel vlabel--d-50"><span>Скидка </span>50%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Панама NOVO</a></div>
                        <div class="prod__prices"><span class="prod__price">898 ₽</span><span
                                class="prod__oldprice">999</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prod col-lg-3">
                <div class="prod__wrap js-tile-gallery"><a
                        class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="product.html"><img
                            class="prod__img js-product-preview-img" src="./img/catalog/product11.jpg"
                            alt="Босоножки T.TACCARDI">
                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                 data-img="./img/catalog/product11.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product16.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product17.jpg"></span><span
                                class="js-tile-gallery-item" data-img="./img/catalog/product18.jpg"></span></div>
                        <div class="js-tile-gallery-dots"></div>
                    </a>
                    <div class="prod__body">
                        <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                     href="catalog.html">Обувь</a><span
                                class="vlabel vlabel--d-30"><span>Скидка </span>30%</span>
                        </div>
                        <div class="prod__title"><a href="product.html">Босоножки T.TACCARDI</a></div>
                        <div class="prod__prices"><span class="prod__price">1 020 ₽</span><span
                                class="prod__oldprice">2 647</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="swiper-body swiper-stl1 swiper-stl1--wide">
        <div class="swiper slider-nav-pag">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><a class="promo__slider-wrap" href="#"> <img
                            class="prodb__img d-none d-md-block" src="./img/main/newcollection1_desktop.jpg"><img
                            class="prodb__img d-md-none" src="./img/main/newcollection1_mobile.jpg"></a></div>
                <div class="swiper-slide"><a class="promo__slider-wrap" href="#"> <img
                            class="prodb__img d-none d-md-block" src="./img/main/newcollection1_desktop.jpg"><img
                            class="prodb__img d-md-none" src="./img/main/newcollection1_mobile.jpg"></a></div>
                <div class="swiper-slide"><a class="promo__slider-wrap" href="#"> <img
                            class="prodb__img d-none d-md-block" src="./img/main/newcollection1_desktop.jpg"><img
                            class="prodb__img d-md-none" src="./img/main/newcollection1_mobile.jpg"></a></div>
            </div>
        </div>
        <div class="swiper-nav">
            <div class="swiper-button swiper-prev">
                <svg class="icon icon-slide-left">
                    <use xlink:href="img/symbol-defs.svg#icon-slide-left"></use>
                </svg>
            </div>
            <div class="swiper-button swiper-next">
                <svg class="icon icon-slide-right">
                    <use xlink:href="img/symbol-defs.svg#icon-slide-right"></use>
                </svg>
            </div>
        </div>
        <div class="swiper-pagination swiper-pagination--md-outer"></div>
    </div>

    <div class="imgcards">
        <div class="imgcards__row row">
            <div class="imgcard col-lg-4 c-white"><a class="imgcard__wrap"
                                                     style="background-image: url(./img/cards/card_pajamas.jpg)"
                                                     href="promotion.html">
                    <div class="imgcard__body">
                        <div class="imgcard__text"><strong>ПИЖАМНАЯ</strong><i>РАСПРОДАЖА</i></div>
                    </div>
                </a></div>
            <div class="imgcard col-lg-4"><a class="imgcard__wrap"
                                             style="background-image: url(./img/cards/card_swimsuits.jpg)"
                                             href="promotion.html">
                    <div class="imgcard__body">
                        <div class="imgcard__text"><i>СКИДКИ</i><strong>НА КУПАЛЬНИКИ</strong></div>
                    </div>
                </a></div>
            <div class="imgcard col-lg-4 c-white"><a class="imgcard__wrap"
                                                     style="background-image: url(./img/cards/card_fitnes.jpg)"
                                                     href="promotion.html">
                    <div class="imgcard__body">
                        <div class="imgcard__text"><strong>ВСЕ ДЛЯ</strong><i>ФИТНЕСА </i></div>
                    </div>
                </a></div>
        </div>

    </div>
    <div class="prods-new">
        <div class="section-title">Новинки</div>
        <div class="i-parent">
            <div class="prods-carousel-head"><a class="vlabel vlabel--border" href="catalog.html">Одежда</a><a
                    class="link link--blue" href="#">Все новинки одежды</a>
                <div class="prods-carousel-nav swiper-nav">
                    <div class="swiper-button swiper-prev">
                        <svg class="icon icon-chevron-left">
                            <use xlink:href="#"></use>
                        </svg>
                    </div>
                    <div class="swiper-button swiper-next">
                        <svg class="icon icon-chevron-right">
                            <use xlink:href="img/symbol-defs.svg#icon-chevron-right"></use>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="prods-carousel-wrap">
                <div class="prods-carousel-body swiper-body">
                    <div class="prods-carousel slider-products-6 swiper">
                        <div class="swiper-wrapper">
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct1.jpg" alt="Бомбер">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct1.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">Бомбер</a></div>
                                        <div class="prod__prices"><span class="prod__price">1 280 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct2.jpg" alt="Плащ TAIFUN">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct2.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">Плащ TAIFUN</a></div>
                                        <div class="prod__prices"><span class="prod__price">28 200 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct3.jpg" alt="ТОП">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct3.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">ТОП</a></div>
                                        <div class="prod__prices"><span class="prod__price">1 800 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct4.jpg" alt="Спортивный костюм">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct4.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">Спортивный костюм</a></div>
                                        <div class="prod__prices"><span class="prod__price">1 530 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct5.jpg" alt="Футболка Roly">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct5.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">Футболка Roly</a></div>
                                        <div class="prod__prices"><span class="prod__price">971 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct1.jpg" alt="Бомбер">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct1.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">Бомбер</a></div>
                                        <div class="prod__prices"><span class="prod__price">1 280 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct2.jpg" alt="Плащ TAIFUN">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct2.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">Плащ TAIFUN</a></div>
                                        <div class="prod__prices"><span class="prod__price">28 200 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-slide swiper-slide">
                                <div class="prod__wrap js-tile-gallery"><a
                                        class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                            class="prod__img js-product-preview-img"
                                            src="./img/catalog/newproduct3.jpg" alt="ТОП">
                                        <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                 data-img="./img/catalog/newproduct3.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product16.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product17.jpg"></span><span
                                                class="js-tile-gallery-item"
                                                data-img="./img/catalog/product18.jpg"></span></div>
                                        <div class="js-tile-gallery-dots"></div>
                                    </a>
                                    <div class="prod__body">
                                        <div class="prod__title"><a href="product.html">ТОП</a></div>
                                        <div class="prod__prices"><span class="prod__price">1 800 ₽</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="prods-new__row row">
            <div class="prods__new-col col-lg-6 i-parent">
                <div class="prods-carousel-head"><a class="vlabel vlabel--border" href="catalog.html">Обувь</a><a
                        class="link link--blue" href="#">Все новинки обуви</a>
                    <div class="prods-carousel-nav swiper-nav">
                        <div class="swiper-button swiper-prev">
                            <svg class="icon icon-chevron-left">
                                <use xlink:href="#"></use>
                            </svg>
                        </div>
                        <div class="swiper-button swiper-next">
                            <svg class="icon icon-chevron-right">
                                <use xlink:href="img/symbol-defs.svg#icon-chevron-right"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="prods-carousel-wrap">
                    <div class="prods-carousel-body swiper-body">
                        <div class="prods-carousel slider-products-3 swiper">
                            <div class="swiper-wrapper">
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/newproduct6.jpg" alt="Кроссовки Nike">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/newproduct6.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Кроссовки Nike</a></div>
                                            <div class="prod__prices"><span class="prod__price">3 300 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/newproduct7.jpg" alt="Балетки">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/newproduct7.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Балетки</a></div>
                                            <div class="prod__prices"><span class="prod__price">644 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/newproduct8.jpg" alt="Шлепанцы">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/newproduct8.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Шлепанцы</a></div>
                                            <div class="prod__prices"><span class="prod__price">1 400 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/product1.jpg" alt="Туфли Майк Мери">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/product1.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Туфли Майк Мери</a>
                                            </div>
                                            <div class="prod__prices"><span class="prod__price">1 853 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prods__new-col col-lg-6 i-parent">
                <div class="prods-carousel-head"><a class="vlabel vlabel--border" href="catalog.html">Аксессуары</a><a
                        class="link link--blue" href="#">Все новинки аксессуаров</a>
                    <div class="prods-carousel-nav swiper-nav">
                        <div class="swiper-button swiper-prev">
                            <svg class="icon icon-chevron-left">
                                <use xlink:href="#"></use>
                            </svg>
                        </div>
                        <div class="swiper-button swiper-next">
                            <svg class="icon icon-chevron-right">
                                <use xlink:href="img/symbol-defs.svg#icon-chevron-right"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="prods-carousel-wrap">
                    <div class="prods-carousel-body swiper-body">
                        <div class="prods-carousel slider-products-3 swiper">
                            <div class="swiper-wrapper">
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/newproduct9.jpg" alt="Браслет родонит плоский">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/newproduct9.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Браслет родонит
                                                    плоский</a></div>
                                            <div class="prod__prices"><span class="prod__price">3 300 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/newproduct10.jpg" alt="Сумка-мессенджер">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/newproduct10.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Сумка-мессенджер</a>
                                            </div>
                                            <div class="prod__prices"><span class="prod__price">644 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/newproduct11.jpg" alt="Оправа для очков Extra">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/newproduct11.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Оправа для очков
                                                    Extra</a></div>
                                            <div class="prod__prices"><span class="prod__price">1 400 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prod-slide swiper-slide">
                                    <div class="prod__wrap js-tile-gallery"><a
                                            class="prod__imgwrap js-tile-gallery-block" href="catalog.html"><img
                                                class="prod__img js-product-preview-img"
                                                src="./img/catalog/product6.jpg" alt="Брошь СТРЕКОЗА">
                                            <div class="js-tile-gallery-items"><span class="js-tile-gallery-item"
                                                                                     data-img="./img/catalog/product6.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product16.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product17.jpg"></span><span
                                                    class="js-tile-gallery-item"
                                                    data-img="./img/catalog/product18.jpg"></span></div>
                                            <div class="js-tile-gallery-dots"></div>
                                        </a>
                                        <div class="prod__body">
                                            <div class="prod__title"><a href="product.html">Брошь СТРЕКОЗА</a></div>
                                            <div class="prod__prices"><span class="prod__price">139 ₽</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="imgcards">
        <div class="imgcards__row row">
            <div class="imgcard imgcard--big imgcard--big--pullright col-lg-6 c-white"><a class="imgcard__wrap"
                                                                                          style="background-image: url(./img/cards/card_bags.jpg)"
                                                                                          href="#">
                    <div class="imgcard__body">
                        <div class="imgcard__text--pullright">
                            <div class="imgcard__text imgcard__text--right"><strong>РЮКЗАКИ /
                                    СУМКИ</strong><i>ЧЕМОДАНЫ</i>
                                <svg class="icon icon-arrow-right">
                                    <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </a></div>
            <div class="imgcard imgcard--big col-lg-6 c-white"><a class="imgcard__wrap"
                                                                  style="background-image: url(./img/cards/card_glasses.jpg)"
                                                                  href="#">
                    <div class="imgcard__body">
                        <div class="imgcard__text"><strong>НОВОЕ ПОСТУПЛЕНИЕ</strong><i>ОЧКИ</i>
                            <svg class="icon icon-arrow-right">
                                <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                            </svg>
                        </div>
                    </div>
                </a></div>
        </div>

    </div>
    <div class="onmap">
        <div class="section-title">Магазины на карте</div>
        <div class="onmap__row row">
            <div class="onmap__left col-lg">
                <div class="onmap__items" data-simplebar data-simplebar-auto-hide="false">
                    <div class="onmap__items-wrap">
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                        <div class="onmap__item">
                            <div class="onmap__item-wrap">
                                <div class="onmap__item-body"><strong class="onmap__item-title">Название
                                        магазина</strong><span class="onmap__item-type">Магазин одежды</span><span
                                        class="onmap__item-address">ул. Ивановская, 10, <br/>1 эт, бут 104</span>
                                </div>
                                <div class="onmap__item-see"><a class="link link--more"><span>Посмотреть</span>
                                        <svg class="icon icon-arrow-right">
                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-right"></use>
                                        </svg>
                                    </a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="onmap__right col-lg-auto">
                <div class="onmap__map"><img src="./img/map.png"></div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="footer__logo"><a class="logo" href="index.html"><img class="logo__img"
                                                                         src="./img/logo.svg"><span
                    class="logo__caption">Маркетплейс <br>одежды и обуви</span></a></div>
        <div class="footer__row row">
            <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                <div class="footer__title"><a href="catalog.html">Каталог</a></div>
                <ul class="footer__list">
                    <li><a href="catalog.html">Одежда</a></li>
                    <li><a href="catalog.html">Обувь</a></li>
                    <li><a href="catalog.html">Аксессуары</a></li>
                </ul>
            </div>
            <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                <div class="footer__title"><a href="#">Правила</a></div>
                <ul class="footer__list">
                    <li><a href="#">Оплата и возврат</a></li>
                    <li><a href="#">Доставка</a></li>
                </ul>
            </div>
            <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                <div class="footer__title"><a href="lk.html">Личный кабинет</a></div>
                <ul class="footer__list">
                    <li><a href="#">Кабинет магазина</a></li>
                    <li><a href="lk.html">Кабинет покупателя</a></li>
                </ul>
            </div>
            <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                <div class="footer__title"><a href="#">Сотрудничество</a></div>
                <ul class="footer__list">
                    <li><a href="#">Магазинам</a></li>
                    <li><a href="#">Инвесторам</a></li>
                </ul>
            </div>
            <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl col-xxl-2">
                <div class="footer__title"><a href="#">Помощь</a></div>
                <ul class="footer__list">
                    <li><a href="#">FAQ</a></li>
                </ul>
            </div>
            <div class="footer__contacts footer__col col-sm-6 col-md-4 col-lg-auto col-xl-auto col-xxl-2">
                <div class="footer__title"><a href="#">Контакты</a></div>
                <div class="footer__contact footer__address">
                    <span>г. Владивосток</span><span>ул. Уткинская 19, офис 3</span>
                </div>
                <div class="footer__contact"><a href="tel:74232100300">+7 423 2100-300</a></div>
            </div>
        </div>

        <div class="footer__bottom">
            <div class="footer__bottom-row row justify-content-between">
                <div class="col-md-6">
                    <p>© 2022 ООО «Витрина». Все права защищены.</p>
                    <p>ИНН 2536333848 <br/>ОГРН 1222500013240</p>
                </div>
                <div class="col-md-auto tar">
                    <p><a href="#">Политика конфиденциальности</a></p>
                    <p><a href="#">Оферта</a></p>
                </div>
            </div>

        </div>
    </footer>
    <div class="hide">
        <div class="popup pform" id="my-location">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header mb-0"><span class="pform__title">Ваш город</span></div>
                <div class="pform__body">
                    <div class="location">
                        <div class="location__current link link--icon">
                            <svg class="icon icon-location">
                                <use xlink:href="img/symbol-defs.svg#icon-location"></use>
                            </svg>
                            <span>Владивосток</span>
                        </div>
                        <span class="location__change" data-fancybox data-src="#city-select">Изменить</span>
                        <div class="hide">
                            <div class="location__select popup pform" id="city-select">
                                <div class="pform__header mb-0"><span class="pform__title">Выберите город</span></div>
                                <div class="pform__body">
                                    <div class="radiocity__items">
                                        <label class="radiocity">
                                            <div class="radiocity__caption link link--icon">
                                                <svg class="icon icon-location">
                                                    <use xlink:href="img/symbol-defs.svg#icon-location"></use>
                                                </svg>
                                                <span>Владивосток</span>
                                            </div>
                                            <input type="radio" name="city_id" value="1" checked>
                                            <div class="radiocity__dot"></div>
                                        </label>
                                        <label class="radiocity">
                                            <div class="radiocity__caption link link--icon">
                                                <svg class="icon icon-location">
                                                    <use xlink:href="img/symbol-defs.svg#icon-location"></use>
                                                </svg>
                                                <span>Артём</span>
                                            </div>
                                            <input type="radio" name="city_id" value="2">
                                            <div class="radiocity__dot"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="popup pform" id="auth-phone-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Авторизация</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">Введите номер телефона</div>
                        <span class="pform__body-subtitle">Мы отправим код в СМС</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Телефон</label>
                        <input class="form-control" type="tel" placeholder="+7 ___-___-__-__">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#auth-phone-code-popup">
                            Получить код
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a class="c-text" href="#" data-fancybox-otherclose
                                              data-src="#register-email-popup">Зарегистрироваться с помощью почты</a>
                </div>
            </form>
        </div>
        <div class="popup pform" id="auth-phone-code-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Введите код</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">На указанный вами номер <br/>телефона отправлен код</div>
                        <span class="pform__body-subtitle">Введите его в поле ниже</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Код</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#register-who">Отправить
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a href="#">Не пришел код? </a></div>
            </form>
        </div>
        <div class="popup popup--w-auto pform" id="register-email-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Регистрация</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">Введите адрес почты</div>
                        <span class="pform__body-subtitle">Мы отправим письмо с кодом</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Email</label>
                        <input class="form-control" type="email">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#register-email-code-popup"
                                data-preclose>Получить код
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a class="c-text" href="#" data-fancybox-otherclose
                                              data-src="#register-phone-popup">Зарегистрироваться с помощью телефона</a>
                </div>
            </form>
        </div>
        <div class="popup pform" id="register-email-code-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Введите код</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">На указанный вами адрес почты <br/>отправлен код подтверждения
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Код</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#register-who">Отправить
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a href="#">Не пришел код? </a></div>
            </form>
        </div>
        <div class="popup pform" id="register-phone-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Регистрация</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">Введите номер телефона</div>
                        <span class="pform__body-subtitle">Мы отправим код в СМС</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Телефон</label>
                        <input class="form-control" type="tel" placeholder="+7 ___-___-__-__">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#register-phone-code-popup"
                                data-preclose>Получить код
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a class="c-text" href="#" data-fancybox-otherclose
                                              data-src="#register-email-popup">Зарегистрироваться с помощью почты</a>
                </div>
            </form>
        </div>
        <div class="popup pform" id="register-phone-code-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Введите код</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">На указанный вами номер <br/>отправлен код подтверждения</div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Код</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#register-who">Отправить
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a href="#">Не пришел код?</a></div>
            </form>
        </div>
        <div class="popup popup--w-auto pform" id="register-who">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Кто вы?</span></div>
                <div class="pform__body">
                    <div class="regtypes"><a class="regtype" href="lk.html">
                            <div class="regtype__iconwrap">
                                <svg class="icon icon-buyer">
                                    <use xlink:href="img/symbol-defs.svg#icon-buyer"></use>
                                </svg>
                            </div>
                            <div class="span regtype__title">Я покупатель</div>
                        </a><a class="regtype" href="anketa.html">
                            <div class="regtype__iconwrap">
                                <svg class="icon icon-store">
                                    <use xlink:href="img/symbol-defs.svg#icon-store"></use>
                                </svg>
                            </div>
                            <div class="span regtype__title">Магазин</div>
                        </a></div>
                </div>
            </form>
        </div>
        <div class="popup pform" id="register-in-cart">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Регистрация</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">Для совершения покупок пройдите регистрацию</div>
                        <span class="pform__body-subtitle">Заполните поля</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Имя</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Телефон</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Email</label>
                        <input class="form-control" type="mail">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#register-phone-popup"
                                data-preclose>Отправить
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="popup pform" id="passreset-phone-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Восстановить пароль</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">Введите номер телефона</div>
                        <span class="pform__body-subtitle">Мы отправим код в СМС</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Телефон</label>
                        <input class="form-control" type="tel" placeholder="+7 ___-___-__-__">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose
                                data-src="#passreset-phone-code-popup">Получить код
                        </button>
                    </div>
                </div>
                <div class="pform__footer">
                    <button class="link--default c-text" type="button" data-fancybox-otherclose
                            data-src="#passreset-email-popup">Восстановить с помощью почты
                    </button>
                </div>
            </form>
        </div>
        <div class="popup pform" id="passreset-phone-code-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Восстановить пароль</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">На указанный вами номер <br/>телефона отправлен код</div>
                        <span class="pform__body-subtitle">Введите его в поле ниже</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Код</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#passreset-new">Отправить
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a href="#">Не пришел код? </a></div>
            </form>
        </div>
        <div class="popup pform" id="passreset-email-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Восстановить пароль</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">Введите email адрес</div>
                        <span class="pform__body-subtitle">Мы отправим код в письме</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Email</label>
                        <input class="form-control" type="mail" placeholder="mail@mail.ru">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose
                                data-src="#passreset-email-code-popup">Получить код
                        </button>
                    </div>
                </div>
                <div class="pform__footer">
                    <button class="link--default c-text" type="button" data-fancybox-otherclose
                            data-src="#passreset-phone-popup">Восстановить с помощью телефона
                    </button>
                </div>
            </form>
        </div>
        <div class="popup pform" id="passreset-email-code-popup">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Восстановить пароль</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">На указанный вами номер <br/>телефона отправлен код</div>
                        <span class="pform__body-subtitle">Введите его в поле ниже</span>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Код</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#passreset-new">Отправить
                        </button>
                    </div>
                </div>
                <div class="pform__footer"><a href="#">Не пришел код? </a></div>
            </form>
        </div>
        <div class="popup pform" id="passreset-new">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Восстановить пароль</span></div>
                <div class="pform__body">
                    <div class="pform__body-head">
                        <div class="pform__body-title">Введите новый пароль</div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Новый пароль</label>
                        <input class="form-control" type="text" placeholder="***************">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Подтвердите пароль</label>
                        <input class="form-control" type="text" placeholder="***************">
                    </div>
                    <div class="form-group">
                        <button class="btn" type="button" data-fancybox-otherclose data-src="#passreset-success">
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="popup pform" id="passreset-success">
            <form class="pform__body fw webform" method="post" action="">
                <div class="pform__header"><span class="pform__title">Восстановить пароль</span></div>
                <div class="pform__body">
                    <div class="pform__body-head mb-0">
                        <div class="pform__body-title">Ваш пароль изменен</div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
