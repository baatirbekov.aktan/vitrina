<div id="category_products" class="prods__row row i-catview-container">
    @foreach($products as $product)
        {{--                      Класс для стилизации      prod--green--}}
        <div class="prod col-lg-4  ">
            <div class="prod__wrap js-tile-gallery">
                <a class="prod__imgwrap js-tile-gallery-block js-tile-gallery-slider" href="#">
                    <img class="prod__img js-product-preview-img"
                         src="{{ asset('/storage/' . $product->productImages->first()->image) }}"
                         alt="#">
                    <div class="js-tile-gallery-items">
                        @foreach($product->productImages as $image)
                            <span class="js-tile-gallery-item"
                                  data-img="{{ asset('/storage/' . $image->image) }}">
                                                </span>
                        @endforeach
                    </div>
                    <div class="js-tile-gallery-dots"></div>
                </a>
                <div class="prod__body">
                    <div class="prod__labels"><a class="prod__section vlabel vlabel--border"
                                                 href="">Одежда</a><span
                            class="vlabel vlabel--d-50"><span>Скидка </span>50%</span>
                    </div>
                    <div class="prod__title">
                        <a href="">{{ $product->name }}</a>
                    </div>
{{--                    @dd($product->options)--}}
{{--                    @foreach($product->options as $option)--}}
{{--                        <h4>{{ $option[0] }}</h4>--}}
{{--                        @foreach(\App\Models\Option::find($option[0])->otpionValues as $value)--}}
{{--                            @php(dd($value))--}}
{{--                            <p>{{ $value }}</p>--}}
{{--                        @endforeach--}}
{{--                    @endforeach--}}
                    <div class="prod__prices"><span class="prod__price">4 330 ₽</span><span
                            class="prod__oldprice">8 398</span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    {{ $products->onEachSide(1)->links('vendor.pagination.default') }}
</div>
