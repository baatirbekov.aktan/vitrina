@extends('layouts.client')

@section('content')
    <div class="shoplogos">
        <div class="container">
            <div class="shoplogos__wrap row">
                <div class="shoplogos__left col-xxl-auto">
                    <span class="section-title">Новые <br/>магазины</span>
                </div>
                <div class="shoplogos__right col-xxl">
                    <div class="swiper-body">
                        <div class="shoplogos-carousel mswiper">
                            <div class="shoplogos__row row mswiper-wrapper">
                                @foreach($shops as $shop)
                                    <div class="shoplogo col-auto mswiper-slide">
                                        <a class="shoplogo__imgwrap" href="#">
                                            <img src="{{ asset('/storage/' . $shop->image) }}" width="85" height="85"
                                                 alt="">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="navbtns">
        <div class="container">
            <div class="navbtn__list">
                @foreach($categories as $value)
                    <a class="navbtn btn btn--nav @if(request()->route('category')->id == $value->id) btn--nav--active @endif"
                       href="{{ route('categories.show', ['category' => $value]) }}">
                        {{ $value->name }}
                    </a>
                    <input type="hidden" id="show_category_id" value="{{ $category->id }}">
                @endforeach
            </div>
        </div>
    </div>
    <div class="catbody">
        <div class="container">
            <div class="catrow row">
                {{--                <form action="" id="filter_form">--}}
                {{--                    @csrf--}}
                <div class="catside col-lg-auto">
                    <div class="catside__body">
                        <div class="sidebox sidebox--category">
                            <div class="sidebox__title">Категория</div>
                            <div class="sidebox__body">
                                <div class="link__list">
                                    <a class="link link--icon link--back" href="#">

                                        <span>{{ $category->name }}</span>

                                    </a>
                                    {{--                                    <a class="link link--icon link--back" href="">--}}
                                    {{--                                        <svg class="icon icon-arrow-left">--}}
                                    {{--                                            <use xlink:href="img/symbol-defs.svg#icon-arrow-left"></use>--}}
                                    {{--                                        </svg>--}}
                                    {{--                                        <span>Женщинам</span>--}}
                                    {{--                                        <svg class="icon icon-close">--}}
                                    {{--                                            <use xlink:href="img/symbol-defs.svg#icon-close"></use>--}}
                                    {{--                                        </svg>--}}
                                    {{--                                    </a>--}}
                                </div>
                            </div>
                        </div>
                        <form action="" id="filter_form">

                            <div class="filters i-filters-body">
                                <div class="filters__title d-lg-none">Фильтры</div>
                                <div class="sidebox">
                                    <div class="sidebox__title">Тип</div>
                                    <div class="sidebox__body check__wrap i-parent">
                                        <div class="check__search">
                                            <input class="check__search-input form-control i-search-input">
                                            <i class="fa-solid fa-magnifying-glass icon icon-search"></i>
                                        </div>
                                        <div class="check__list checkboxstyles i-search-list" data-simplebar
                                             data-simplebar-auto-hide="false">
                                            @foreach($category->subcategories as $subcategory)
                                                <div class="check__item checkboxstyle">
                                                    <label class="check__label">
                                                        <input @checked(in_array($subcategory->id, request('subcategories') ?? [])) type="checkbox"
                                                               value="{{ $subcategory->id }}" name="subcategories[]"
                                                               class="subcategories_ids">
                                                        <span class="check__caption">{{ $subcategory->name }}</span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <span class="check__toggle" data-show="Посмотреть все" data-hide="Скрыть">Посмотреть все</span>
                                    </div>
                                </div>
                                <div class="sidebox">
                                    <div class="sidebox__body">
                                        <div class="check__list checkboxstyles checkboxstyles--b">
                                            <div class="check__item checkboxstyle">
                                                <label class="check__label">
                                                    <input type="checkbox"><span class="check__caption">Новинки</span>
                                                </label>
                                            </div>
                                            <div class="check__item checkboxstyle">
                                                <label class="check__label">
                                                    <input type="checkbox"><span class="check__caption">Скидки</span>
                                                </label>
                                            </div>
                                            <div class="check__item checkboxstyle">
                                                <label class="check__label">
                                                    <input type="checkbox"><span class="check__caption">Местный производитель</span>
                                                </label>
                                            </div>
                                            <div class="check__item checkboxstyle">
                                                <label class="check__label">
                                                    <input type="checkbox"><span class="check__caption">Доставка</span>
                                                </label>
                                            </div>
                                            <div class="check__item checkboxstyle">
                                                <label class="check__label">
                                                    <input type="checkbox"><span class="check__caption">Примерка</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @include('client.categories.options', ['options' => $options])
                                <div class="sidebox sidebox--noborder">
                                    <div class="sidebox__body">
                                        <div class="sidebox__btns">
                                            <button class="btn">Применить</button>
                                            <a class="link" href="#">Сбросить</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="filters__close i-filters-close d-lg-none">
                                    {{--                                <svg class="icon icon-close">--}}
                                    {{--                                    <use xlink:href="img/symbol-defs.svg#icon-close"></use>--}}
                                    {{--                                </svg>--}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                {{--                </form>--}}
                <div class="catcontent col-lg i-catview-parent">
                    <div class="cattools">
                        <div class="cattools__filter i-filters-toggle d-lg-none">
                            {{--                            <svg class="icon icon-filters">--}}
                            {{--                                <use xlink:href="img/symbol-defs.svg#icon-filters"></use>--}}
                            {{--                            </svg>--}}
                        </div>
                        <div class="select-custom select-custom--noborder unselectable">
                            <select>
                                <option value="Новое">Новое</option>
                                <option value="По размеру скидки">По размеру скидки</option>
                                <option value="Сначала дешевле">Сначала дешевле</option>
                                <option value="Сначала дороже">Сначала дороже</option>
                            </select>
                        </div>
                        <div class="catview">
                            <div class="catview__btn i-catview-toggle v1 is-active"><span
                                    class="catview__btn--v3"><span></span><span></span><span></span></span></div>
                            <div class="catview__btn i-catview-toggle v2"><span
                                    class="catview__btn--v6"><span>
                                    </span><span> </span><span> </span><span> </span><span> </span><span></span></span>
                            </div>
                        </div>
                    </div>
                    @include('client.products.index', ['products' => $products])
                </div>
            </div>
        </div>
    </div>
@endsection
