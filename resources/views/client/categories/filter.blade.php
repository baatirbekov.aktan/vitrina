@foreach($options as $option)
    <div class="sidebox">
        <div class="sidebox__title">{{ $option->name }}</div>
        <div class="sidebox__body check__wrap i-parent">
            <div class="check__search">
                <input class="check__search-input form-control i-search-input">
                <i class="fa-solid fa-magnifying-glass icon icon-search"></i>
            </div>
            <div class="check__list checkboxstyles i-search-list" data-simplebar
                 data-simplebar-auto-hide="false">
                @foreach($option->optionValues as $optionValue)
                    <div class="check__item checkboxstyle">
                        <label class="check__label">
                            <input type="checkbox"><span
                                class="check__caption">{{ $optionValue->name }}</span>
                        </label>
                    </div>
                @endforeach
            </div>
            <span class="check__toggle" data-show="Посмотреть все" data-hide="Скрыть">Посмотреть все</span>
        </div>
    </div>
@endforeach
