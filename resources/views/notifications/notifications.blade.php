@if (session('success'))
    <script>
        const message = "<?php echo session('success') ?>";
        document.addEventListener("DOMContentLoaded", () => {
            Swal.fire({
                icon: 'success',
                title: message,
                showConfirmButton: false,
                timer: 1500
            })
        });
    </script>
@endif

@if (session('error'))
    <script type="text/javascript">
        const message = "<?php echo session('error') ?>";
        document.addEventListener("DOMContentLoaded", () => {
            Swal.fire({
                icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 1500
            })
        });
    </script>
@endif
