<label for="{{ $id }}" class="form-label">{{ $label }}</label>

<input
    @if($errors->has($name))
        {{ $attributes->merge(['class' => 'form-control is-invalid']) }}
    @else
        {{ $attributes->merge(['class' => 'form-control']) }}
    @endif
    placeholder="{{ $placeholder }}"
    name="{{ $name }}"
    type="{{ $type }}"
    id="{{ $id }}"
    value="{{ $value }}">

@error($name)
<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
@enderror
