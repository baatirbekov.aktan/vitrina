<label for="{{ $id }}" class="form-label">{{ $label }}</label>
<select
    @if($errors->has(str_replace(['[',']'], '', $name)))
        {{ $attributes->merge(['class' => 'form-select is-invalid']) }}
    @else
        {{ $attributes->merge(['class' => 'form-select']) }}
    @endif id="{{ $id }}"
    name="{{ $name }}"
    class="form-select">
    @if($placeholder)
        <option value="">{{ $placeholder }}</option>
    @endif
    {{ $slot }}
</select>

@error(str_replace(['[',']'], '', $name))
<span class="invalid-feedback" role="alert">
    <strong>{{ $message }}</strong>
</span>
@enderror
