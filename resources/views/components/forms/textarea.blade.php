<div class="form-floating">
    <textarea
        @if($errors->has($name))
            {{ $attributes->merge(['class' => 'form-control is-invalid']) }}
        @else
            {{ $attributes->merge(['class' => 'form-control']) }}
        @endif
        name="{{ $name }}"
        placeholder="Leave a comment here" id="{{ $id }}"
        style="height: 100px">{{ $value }}</textarea>
    <label for="{{ $id }}">{{ $label }}</label>
    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
