<div class="d-flex">
    <select name="limit" form="search" id="limit" style="width: 100px;" class="form-select">
        @foreach($types as $type)
            <option @if(Session::get("{$table}.limit") == $type ) selected @endif value="{{ $type }}">{{ $type }}</option>
        @endforeach
    </select>
    <label class="mt-2 mx-2" for="limit">записей</label>
</div>
<hr>
