<div style="display: flex; justify-content: space-between;">
    <div>
        {{ $label }}
    </div>
    @if(Session::get("{$table}.order_by") == $name)
        <button data-order-type="{{ Session::get("{$table}.order_type") }}" data-name="{{ $name }}"
                class="btn btn-sm btn-outline-secondary border-0 sort">
            <i @if(Session::get("{$table}.order_type") == 'ASC') style="color: black"
               @endif class="bi bi-arrow-down down"></i>
            <i @if(Session::get("{$table}.order_type") == 'DESC') style="color: black"
               @endif class="bi bi-arrow-up up"></i>
        </button>
    @else
        <button data-order-type="" data-name="{{ $name }}"
                class="btn btn-sm btn-outline-secondary border-0 sort">
            <i class="bi bi-arrow-down down"></i>
            <i class="bi bi-arrow-up up"></i>
        </button>
    @endif
</div>
