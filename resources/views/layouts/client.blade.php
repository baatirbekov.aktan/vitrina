<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Главная | ВИТРИНА - маркетплейс одежды и обуви</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="https://use.fontawesome.com/releases/v6.2.0/js/all.js" crossorigin="anonymous"></script>

    <link href="{{ asset('css/vendors.bdf213427bafb9a38bbc.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.a72417657e89e7791e2e.css') }}" rel="stylesheet">
    <!-- Scripts -->
    @vite(['resources/js/app.js'])
</head>
<body class="home">

<div class="l-wrap">
    <div class="topline">
        <div class="container">
            <div class="topline__row row">
                <div class="topline__left col-auto">
                    <a class="link link--icon" href="#" data-fancybox data-src="#my-location">
{{--                        <svg class="icon icon-location">--}}
{{--                            <use xlink:href="img/symbol-defs.svg#icon-location"></use>--}}
{{--                        </svg>--}}
                        <span>Владивосток</span>
                    </a>
                </div>
                <div class="topline__right col-auto">
                    <a class="link" href="#">Сотрудничество</a>
                    <a class="link" href="#">Помощь</a>
                </div>
            </div>
        </div>
    </div>
    <header class="top">
        <div class="container">
            <div class="top__row row">
                <div class="top__logo col-auto"><a class="logo" href="#">
                        <img class="logo__img"
                             src="{{ asset('/storage/images/logo/logo.svg') }}"
                             width="110"
                             height="40"
                             alt="">
                        <span class="logo__caption">Маркетплейс <br>одежды и обуви</span>
                    </a>
                </div>
                <div class="top__catalog col-md-auto"><a class="nav-cat" href="#">
                        <svg class="icon icon-catalog">
                            <use xlink:href="#"></use>
                        </svg>
                        <span>Каталог</span></a></div>
                <div class="top__search col-md"><a class="top__search-back d-md-none" href="#"
                                                   onclick="history.back();return false;">
                        <svg class="icon icon-chevron-left">
                            <use xlink:href="#"></use>
                        </svg>
                    </a>
                    <div class="input-group">
                        <div class="select-custom unselectable">
                            <select class="form-select">
                                <option selected>По товарам</option>
                                <option>По категориям</option>
                            </select>
                        </div>
                        <button class="input-group-icon" type="submit"
                                onclick="window.location='catalog-search.html'">
{{--                            <svg class="icon icon-search">--}}
{{--                                <use xlink:href="img/symbol-defs.svg#icon-search"></use>--}}
{{--                            </svg>--}}
                        </button>
                        <input class="form-control" type="text" placeholder="Поиск на Витрине">
                    </div>
                </div>
                <div class="top__tools col-auto"><a class="minicart" href="#">
                        <div class="minicart__icon icount">
{{--                            <svg class="icon icon-cart">--}}
{{--                                <use xlink:href="img/symbol-defs.svg#icon-cart"></use>--}}
{{--                            </svg>--}}
                            <span class="minicart__count icount__caption">12</span>
                        </div>
                        <span class="minicart__cost"><span id="minicart-cost">101 999</span>₽</span></a>
                    <div class="top__auth is_active"><a class="link link--icon" href="#">
{{--                            <svg class="icon icon-profile">--}}
{{--                                <use xlink:href="img/symbol-defs.svg#icon-profile"></use>--}}
{{--                            </svg>--}}
                            <span>Анастасия <br/>Смирнова</span></a></div>
                    <div class="top__hamb col-auto">
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="promo">
        <div class="container">
            <div class="swiper-body swiper-stl1">
                <div class="promo__slider swiper slider-nav-pag">
                    <div class="swiper-wrapper">
                        <div class="promo-slide swiper-slide pslide1">
                            <a class="promo__slider-wrap" href="#">
                                <img class="promo__slider-img l-desktop" src="{{ asset('/storage/images/main/promo1_desktop.jpg') }}" alt="">
                                <img class="promo__slider-img l-mobile" src="{{ asset('/storage/images/main/promo1_mobile.jpg') }}" alt="">
                            </a>
                        </div>
                        <div class="promo-slide swiper-slide pslide1">
                            <a class="promo__slider-wrap" href="#">
                                <img class="promo__slider-img l-desktop" src="{{ asset('/storage/images/main/promo1_desktop.jpg') }}" alt="">
                                <img class="promo__slider-img l-mobile" src="{{ asset('/storage/images/main/promo1_mobile.jpg') }}" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="swiper-nav swiper-nav--mobile">
                    <div class="swiper-button swiper-prev">
                        <div class="d-md-none">
{{--                            <svg class="icon icon-arrow-left-small">--}}
{{--                                <use xlink:href="img/symbol-defs.svg#icon-arrow-left-small"></use>--}}
{{--                            </svg>--}}
                        </div>
{{--                        <svg class="icon icon-slide-left">--}}
{{--                            <use xlink:href="img/symbol-defs.svg#icon-slide-left"></use>--}}
{{--                        </svg>--}}
                    </div>
                    <div class="swiper-button swiper-next">
                        <div class="d-md-none">
{{--                            <svg class="icon icon-arrow-right-small">--}}
{{--                                <use xlink:href="img/symbol-defs.svg#icon-arrow-right-small"></use>--}}
{{--                            </svg>--}}
                        </div>
{{--                        <svg class="icon icon-slide-right">--}}
{{--                            <use xlink:href="img/symbol-defs.svg#icon-slide-right"></use>--}}
{{--                        </svg>--}}
                    </div>
                </div>
                <div class="swiper-pagination swiper-pagination--mobile"></div>
            </div>
        </div>
    </div>
    @yield('content')
    <footer class="footer mt-5">
        <div class="container">
            <div class="footer__logo">
                <a class="logo" href="#">
                    <img class="logo__img"
                         src="{{ asset('/storage/images/logo/logo.svg') }}"
                         alt="#">
                    <span class="logo__caption">Маркетплейс <br>одежды и обуви</span>
                </a>
            </div>
            <div class="footer__row row">
                <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                    <div class="footer__title"><a href="#">Каталог</a></div>
                    <ul class="footer__list">
                        <li><a href="#">Одежда</a></li>
                        <li><a href="#">Обувь</a></li>
                        <li><a href="#">Аксессуары</a></li>
                    </ul>
                </div>
                <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                    <div class="footer__title"><a href="#">Правила</a></div>
                    <ul class="footer__list">
                        <li><a href="#">Оплата и возврат</a></li>
                        <li><a href="#">Доставка</a></li>
                    </ul>
                </div>
                <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                    <div class="footer__title"><a href="lk.html">Личный кабинет</a></div>
                    <ul class="footer__list">
                        <li><a href="#">Кабинет магазина</a></li>
                        <li><a href="lk.html">Кабинет покупателя</a></li>
                    </ul>
                </div>
                <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl-2">
                    <div class="footer__title"><a href="#">Сотрудничество</a></div>
                    <ul class="footer__list">
                        <li><a href="#">Магазинам</a></li>
                        <li><a href="#">Инвесторам</a></li>
                    </ul>
                </div>
                <div class="footer__col col-sm-6 col-md-4 col-lg-auto col-xl col-xxl-2">
                    <div class="footer__title"><a href="#">Помощь</a></div>
                    <ul class="footer__list">
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
                <div class="footer__contacts footer__col col-sm-6 col-md-4 col-lg-auto col-xl-auto col-xxl-2">
                    <div class="footer__title"><a href="#">Контакты</a></div>
                    <div class="footer__contact footer__address">
                        <span>г. Владивосток</span><span>ул. Уткинская 19, офис 3</span></div>
                    <div class="footer__contact"><a href="tel:74232100300">+7 423 2100-300</a></div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <div class="footer__bottom-row row justify-content-between">
                    <div class="col-md-6">
                        <p>© 2022 ООО «Витрина». Все права защищены.</p>
                        <p>ИНН 2536333848 <br/>ОГРН 1222500013240</p>
                    </div>
                    <div class="col-md-auto tar">
                        <p><a href="#">Политика конфиденциальности</a></p>
                        <p><a href="#">Оферта</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="{{ asset('js/vendors.d95a6e5831869bd5a585.js') }}"></script>
</body>
</html>
