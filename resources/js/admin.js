$(() => {
    $(document).on('click', '#sidebarToggle', function () {
        document.body.classList.toggle('sb-sidenav-toggled');
    })

    $(document).on('click', '.sort', function (e) {
        sort(e);
    })

    $(document).on('click', '.clean-input', function (e) {
        const name = $(this).data('name');
        $(`input[name=${name}]`).val('');

        filter();
    });

    $(document).on('input', '*[form="search"]', function (e) {
        filter();
    });

    $(document).on('click', '.page-item a', function (e) {
        e.preventDefault();
        paginate(e);
    });

    $('.multiple-select').select2({
        closeOnSelect: false,
        multiple: true,
        placeholder: "Выберите варианты",
        theme: 'bootstrap-5',
    });

    $('.single-select').select2({
        multiple: false,
        placeholder: "Выберите варианты",
        theme: 'bootstrap-5',
    });
})


function filter() {
    const form = $('#search');
    const table = form.data('table');
    const data = form.serialize();

    $.ajax({
        method: 'GET',
        url: `/admin/${table}`,
        cache: false,
        data: data
    }).done(successResponse => {
        $(`#${table}-table`).replaceWith(successResponse.table)
        window.history.replaceState({}, table, successResponse.url);
    })
}

function paginate(e) {
    const element = e.currentTarget;
    const url = $(element).attr('href')
    const form = $('#search');
    const table = form.data('table');

    $.ajax({
        method: 'GET',
        url: url,
        cache: false
    }).done(successResponse => {
        $(`#${table}-table`).replaceWith(successResponse.table)
        window.history.replaceState({}, table, successResponse.url);
    })
}

function sort(e) {
    const element = e.currentTarget;
    let type = $(element).data('order-type') ? $(element).data('order-type') : 'DESC';
    const name = $(element).data('name');
    const form = $('#search');
    const table = form.data('table');
    const data = form.serializeArray();

    if (type === 'ASC') {
        type = 'DESC';
    } else {
        type = 'ASC';
    }

    data.push(
        {name: "order_by", value: name},
        {name: "order_type", value: type}
    );

    $.ajax({
        method: 'GET',
        url: `/admin/${table}`,
        cache: false,
        data: $.param(data)
    }).done(successResponse => {
        $(`#${table}-table`).replaceWith(successResponse.table)
        window.history.replaceState({}, table, successResponse.url);
    })
}
