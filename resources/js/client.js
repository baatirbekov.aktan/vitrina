$(() => {
    $(document).on('click', '.subcategories_ids', function () {

        const inputs = $('.subcategories_ids:checked');
        const optionsInputs = $('.options_ids:checked');
        let values = [];

        $.each(inputs, function (index, input) {
            values.push($(input).val());
        });

        let optionsIds = [];

        $.each(optionsInputs, function (index, input) {
            optionsIds.push($(input).val());
        });

        console.log(optionsIds)

        $.ajax({
            method: 'GET',
            url: `/options`,
            cache: false,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: {subcategories_ids: values, options_ids: optionsIds}
        }).done(successResponse => {
            $(`#filter-options`).replaceWith(successResponse.options)
            // window.history.replaceState({}, table, successResponse.url);
        })
    });


    $(document).on('click', '.check__toggle', function () {
        let t = this.closest(".i-parent");
        let n = this;
        t.classList.toggle("i-open");
        n.classList.toggle("active");
        let l = t.classList.contains("i-open");
        n.innerHTML = l ? n.dataset.hide : n.dataset.show;
    });

    $(document).on('submit', '#filter_form', function (e) {
        e.preventDefault();
        const data = $('#filter_form').serialize();
        const categoryId = $('#show_category_id').val();

        $.ajax({
            method: 'GET',
            url: `/categories/${categoryId}`,
            cache: false,
            data: data
        }).done(successResponse => {
            $(`#category_products`).replaceWith(successResponse.products)
            window.history.replaceState({}, "",successResponse.url);
        })
    })

})
