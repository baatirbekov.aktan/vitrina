<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('categories', \App\Http\Controllers\CategoryController::class);

Route::prefix('admin')->name('admin.')->middleware(['auth', 'role:admin'])->group(function () {
    Route::resources([
        'users' => \App\Http\Controllers\Admin\UserController::class,
        'categories' => \App\Http\Controllers\Admin\CategoryController::class,
        'subcategories' => \App\Http\Controllers\Admin\SubcategoryController::class,
        'products' => \App\Http\Controllers\Admin\ProductController::class,
    ]);

    Route::get('/', function () {
        return view('admin.dashboard');
    })->name('dashboard');
});

Route::resource('options', \App\Http\Controllers\OptionController::class);
