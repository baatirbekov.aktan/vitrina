<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\Session;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    /**
     * @return HasMany
     */
    public function subcategories(): HasMany
    {
        return $this->hasMany(Subcategory::class);
    }

    /**
     * @return HasManyThrough
     */
    public function products(): HasManyThrough
    {
        return $this->hasManyThrough(Product::class, Subcategory::class);
    }

    /**
     * @param Builder $query
     * @return void
     */
    public function scopeFilter(Builder $query): void
    {
        $query->when(request('search'), function (Builder $q) {
            $q->where('name', 'LIKE', '%' . request('search') . '%');
        });

    }

    /**
     * @param Builder $query
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function scopeOrder(Builder $query): void
    {
        request()->whenFilled('order_type', function () {
            Session::put("{$this->getTable()}.order_type", request('order_type'));
        });

        request()->whenFilled('order_by', function () {
            Session::put("{$this->getTable()}.order_by", request('order_by'));
        });

        if (request()->filled('limit') && Session::get("{$this->getTable()}.limit") != request()->get('limit')) {
            Session::put("{$this->getTable()}.limit", request('limit'));
        }

        $query->orderBy(
            Session::get("{$this->getTable()}.order_by") ?? 'id',
            Session::get("{$this->getTable()}.order_type") ?? 'DESC'
        );
    }
}
