<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Session;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['subcategory_id', 'shop_id', 'is_new', 'discount', 'price', 'name', 'options'];

    protected $casts = [
        'options' => 'json',
    ];

    /**
     * @return BelongsTo
     */
    public function subcategory(): BelongsTo
    {
        return $this->belongsTo(Subcategory::class);
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @return HasMany
     */
    public function productImages(): HasMany
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @param Builder $query
     * @return void
     */
    public function scopeFilter(Builder $query): void
    {
        $query->when(request('search'), function (Builder $q) {
            $q->where('name', 'ILIKE', '%' . request('search') . '%');
        });

        $query->when(request('subcategories'), function (Builder $q) {
            $q->whereIn('subcategory_id', request('subcategories'));
        });

        $query->when(request('options'), function (Builder $q) {
            foreach (request('options') as $key => $value) {
                $q->whereJsonContains("options->$key", array_map('strval', $value));
            }
        });
    }

    /**
     * @param Builder $query
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function scopeOrder(Builder $query): void
    {
        request()->whenFilled('order_type', function () {
            Session::put("{$this->getTable()}.order_type", request('order_type'));
        });

        request()->whenFilled('order_by', function () {
            Session::put("{$this->getTable()}.order_by", request('order_by'));
        });

        if (request()->filled('limit') && Session::get("{$this->getTable()}.limit") != request()->get('limit')) {
            Session::put("{$this->getTable()}.limit", request('limit'));
        }

        $query->orderBy(
            Session::get("{$this->getTable()}.order_by") ?? 'id',
            Session::get("{$this->getTable()}.order_type") ?? 'DESC'
        );
    }
}
