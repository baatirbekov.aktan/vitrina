<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Option extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'value'];

    /**
     * @return HasMany
     */
    public function optionValues(): HasMany
    {
        return $this->hasMany(OptionValue::class);
    }

    /**
     * @return BelongsToMany
     */
    public function subcategories(): BelongsToMany
    {
        return $this->belongsToMany(Subcategory::class, 'subcategory_option');
    }

    /**
     * @param Builder $query
     * @param $categoryId
     * @return void
     */
    public function scopeGetByCategoryId(Builder $query, $categoryId): void
    {
        $query->select('options.*')
            ->join('subcategory_option', 'options.id', '=', 'subcategory_option.option_id')
            ->join('subcategories', 'subcategory_option.subcategory_id', '=', 'subcategories.id')
            ->where('subcategories.category_id', '=', $categoryId)
            ->distinct();

    }
}
