<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application|JsonResponse
     */
    public function index(Request $request): \Illuminate\Foundation\Application|View|Factory|JsonResponse|Application
    {
        $table = Category::query()->getQuery()->from;

        $categories = Category::query()
            ->filter()
            ->order()
            ->paginate(Session::get("{$table}.limit") ?? 5)
            ->withQueryString();

        Session::put("{$table}.url", $request->fullUrl());

        if ($request->ajax()) {
            return response()->json(
                [
                    'table' => view('admin.categories.table', [
                        'categories' => $categories,
                        'table' => $table
                    ])->render(),
                    'url' => $request->fullUrl()
                ],
                200
            );
        }

        return view('admin.categories.index', [
            'categories' => $categories,
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        //
    }
}
