<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Option;
use App\Models\Subcategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;

class SubcategoryController extends Controller
{

    /**
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application|JsonResponse
     */
    public function index(Request $request): \Illuminate\Foundation\Application|View|Factory|JsonResponse|Application
    {
        $table = Subcategory::query()->getQuery()->from;

        $subcategories = Subcategory::query()
            ->filter()
            ->with(['category', 'gender'])
            ->order()
            ->paginate(Session::get("{$table}.limit") ?? 5)
            ->withQueryString();

        Session::put("{$table}.url", $request->fullUrl());

        if ($request->ajax()) {
            return response()->json(
                [
                    'table' => view('admin.subcategories.table', [
                        'subcategories' => $subcategories,
                        'table' => $table
                    ])->render(),
                    'url' => $request->fullUrl()
                ],
                200
            );
        }

        return view('admin.subcategories.index', [
            'subcategories' => $subcategories,
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Subcategory $subcategory)
    {
        //
    }


    /**
     * @param Subcategory $subcategory
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit(Subcategory $subcategory): \Illuminate\Foundation\Application|View|Factory|Application
    {
        $options = Option::all();
        $categories = Category::all();

        $subcategoryOptions = $subcategory
            ->options()
            ->pluck('options.id')
            ->toArray();

        return view('admin.subcategories.edit', [
            'subcategory' => $subcategory,
            'categories' => $categories,
            'options' => $options,
            'subcategoryOptions' => $subcategoryOptions
        ]);
    }


    /**
     * @param Request $request
     * @param Subcategory $subcategory
     * @return Application|\Illuminate\Foundation\Application|RedirectResponse|Redirector
     */
    public function update(Request $request, Subcategory $subcategory): \Illuminate\Foundation\Application|Redirector|RedirectResponse|Application
    {
        $table = Subcategory::query()->getQuery()->from;

        $subcategory->update($request->all());

        $subcategory->options()->sync($request->get('options'));

        return redirect(Session::get("{$table}.url") ?? route('admin.subcategories.index'))->with('success', 'Успешно создано!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Subcategory $subcategory)
    {
        //
    }
}
