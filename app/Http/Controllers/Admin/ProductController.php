<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Subcategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application|JsonResponse
     */
    public function index(Request $request): \Illuminate\Foundation\Application|View|Factory|JsonResponse|Application
    {
        $table = Product::query()->getQuery()->from;

        $products = Product::query()
            ->filter()
            ->with(['shop', 'subcategory'])
            ->order()
            ->paginate(Session::get("{$table}.limit") ?? 5)
            ->withQueryString();

        Session::put("{$table}.url", $request->fullUrl());

        if ($request->ajax()) {
            return response()->json(
                [
                    'table' => view('admin.products.table', [
                        'products' => $products,
                        'table' => $table
                    ])->render(),
                    'url' => $request->fullUrl()
                ],
                200
            );
        }

        return view('admin.products.index', [
            'products' => $products,
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * @param Product $product
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit(Product $product): \Illuminate\Foundation\Application|View|Factory|Application
    {
        $shops = Shop::all();
        $subcategories = Subcategory::all();

        return view('admin.products.edit', [
            'product' => $product,
            'shops' => $shops,
            'subcategories' => $subcategories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $table = Subcategory::query()->getQuery()->from;
//        dd($request->all());
        $product->update($request->all());

        return redirect(Session::get("{$table}.url") ?? route('admin.products.index'))->with('success', 'Успешно обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        //
    }
}
