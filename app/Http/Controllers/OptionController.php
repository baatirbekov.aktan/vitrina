<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOptionRequest;
use App\Http\Requests\UpdateOptionRequest;
use App\Models\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $options = Option::query()
            ->select('options.*')
            ->join('subcategory_option', 'options.id', '=', 'subcategory_option.option_id')
            ->join('subcategories', 'subcategory_option.subcategory_id', '=', 'subcategories.id')
            ->whereIn('subcategories.id', $request->get('subcategories_ids'))
            ->distinct()
            ->get();


        return response()->json(
            [
                'options' => view('client.categories.options', [
                    'options' => $options,
                    'options_ids' => $request->get('options_ids')
                ])->render(),
            ],
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOptionRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Option $option)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Option $option)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOptionRequest $request, Option $option)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Option $option)
    {
        //
    }
}
