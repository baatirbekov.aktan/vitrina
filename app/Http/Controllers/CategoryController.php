<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use App\Models\Gender;
use App\Models\Option;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request)
    {
        //
    }

    /**
     * @param Category $category
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application|JsonResponse
     */
    public function show(Category $category, Request $request): \Illuminate\Foundation\Application|View|Factory|JsonResponse|Application
    {
        $products = $category->load('subcategories')
            ->products()
            ->filter($request->all())
            ->paginate(15)
            ->withQueryString();

        if ($request->ajax()) {
            return response()->json(
                [
                    'products' => view('client.products.index', [
                        'products' => $products,
                    ])->render(),
                    'url' => $request->fullUrl(),
                ],
                200
            );
        }

        $options = Option::getByCategoryId($category->id)->get();

        return view('client.categories.show', [
            'category' => $category,
            'products' => $products,
            'options' => $options,
            'genders' => Gender::all(),
            'shops' => Shop::all(),
            'categories' => Category::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        //
    }
}
