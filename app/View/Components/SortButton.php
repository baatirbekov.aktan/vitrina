<?php

namespace App\View\Components;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SortButton extends Component
{
    /**
     * The name.
     *
     * @var string
     */
    public string $name;

    /**
     * The label.
     *
     * @var string
     */
    public string $label;

    /**
     * The label.
     *
     * @var string
     */
    public string $table;

    /**
     * @param string $name
     * @param string $label
     * @param string $table
     */
    public function __construct(string $name, string $label, string $table)
    {
        $this->name = $name;
        $this->label = $label;
        $this->table = $table;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('components.sort-button');
    }
}
