<?php

namespace App\View\Components\Forms;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Textarea extends Component
{
    public string $id;

    public string $name;

    public string $label;

    public string $value;

    /**
     * @param string $name
     * @param string $id
     * @param string $value
     * @param string $label
     */
    public function __construct(string $name, string $value = '', string $label = '', string $id = '')
    {
        $this->id = $id;
        $this->label = $label;
        $this->value = $value;
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('components.forms.textarea');
    }
}
