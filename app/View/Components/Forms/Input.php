<?php

namespace App\View\Components\Forms;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Input extends Component
{
    public string $id;

    public string $name;

    public string $label;

    public string $value;

    public string $type;

    public string $placeholder;

    /**
     * @param string $name
     * @param string $placeholder
     * @param string $type
     * @param string $id
     * @param string $value
     * @param string $label
     */
    public function __construct(string $name, string $type, string $value = '', string $label = '', string $placeholder = '', string $id = '')
    {
        $this->id = $id;
        $this->label = $label;
        $this->value = $value;
        $this->name = $name;
        $this->type = $type;
        $this->placeholder = $placeholder;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('components.forms.input');
    }
}
